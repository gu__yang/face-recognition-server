﻿using System;

namespace FaceService.DB
{
    public static class DBCONN
    {
        public static string GetCurrentProjectPath
        {

            get
            {
                return Environment.CurrentDirectory.Replace(@"\bin\Debug", "");//获取具体路径
            }
        }
        public static string DBConn_SqliteLocal = @"Data Source=" + GetCurrentProjectPath + @"\DataBase\DADatabase.db;";
    }

}

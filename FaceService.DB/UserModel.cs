﻿namespace FaceService.DB
{
    /// <summary>
    /// 用户人脸模型
    /// </summary>
    public class UserFeatureModel
    {
        public string userid { get; set; }
        public string username { get; set; }
        public string userfeature { get; set; }
        public int userfeaturesize { get; set; }
        public string userimage { get; set; }
    }
}

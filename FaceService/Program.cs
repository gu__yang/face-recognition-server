using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;
//using Serilog.Events;

namespace FaceService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //增加Log
            Log.Logger = new LoggerConfiguration()
    .MinimumLevel.Debug()//最小的输出单位是Debug级别的
    .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Information)//将Microsoft前缀的日志的最小输出级别改成Information
    .Enrich.FromLogContext()
    .WriteTo.File(Environment.CurrentDirectory.Replace(@"\bin\Debug", "") + "\\Serilogs\\serilog.txt", rollingInterval: RollingInterval.Day, outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {Message:lj}{NewLine}{Exception}")
    .WriteTo.Console(
                    outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Message:lj} {Properties:j}{NewLine}{Exception}")
    .CreateLogger();

            Log.Information("Starting web host");
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()//使用serilog
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}

﻿using FaceService.Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace FaceService.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DBConnectTestController : ControllerBase
    {
        // GET: api/<DBConnectTestController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            TestDB testDB = new TestDB();
            if (testDB.TestDBConnection())
            {
                return new string[] { "success" };
            }
            else
            {
                return new string[] { "fail" };
            }
            

            
        }

        // GET api/<DBConnectTestController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<DBConnectTestController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<DBConnectTestController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<DBConnectTestController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

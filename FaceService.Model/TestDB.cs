﻿using FaceService.DB;
using SqlSugar;
using System;

namespace FaceService.Model
{
    public class TestDB
    {
        public bool TestDBConnection()
        {
            try
            {
                SqlSugarClient db = new SqlSugarClient(new ConnectionConfig()
                {
                    ConnectionString = DBCONN.DBConn_SqliteLocal,//连接符字串
                    DbType = DbType.Sqlite,
                    IsAutoCloseConnection = true,
                    InitKeyType = InitKeyType.Attribute//从特性读取主键自增信息
                });
                db.Queryable<UserFeatureModel>().ToList();

                return true;
            }
            catch (Exception ex)
            {
                //throw;
                return false;
            }

        }
    }
}
